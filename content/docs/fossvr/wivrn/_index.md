---
weight: 200
title: WiVRn
---

# WiVRn

- [WiVRn GitHub repository](https://github.com/Meumeu/WiVRn)

![The WiVRn mascot](https://github.com/Meumeu/WiVRn/blob/master/images/wivrn.svg?raw=true)

> WiVRn lets you run OpenXR applications on a computer and display them on a standalone headset.

It's very similar in purpose to [Monado](/docs/fossvr/monado/), but for standalone VR headsets.

Current limitations are a fixed bitrate and network protocol streaming only, please ensure your headset is on a stable network or utilize an ethernet adapter to your headset to operate in a quasi-wired mode.

Should you utilize [Envision](/docs/fossvr/envision/) for WiVRn setup, please ensure your APK and WiVRn server match as closely as possible. Check the WiVRn github [actions](https://github.com/Meumeu/WiVRn/actions) for an APK matching your server build to install to your headset.