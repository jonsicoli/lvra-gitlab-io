---
weight: 50
title: Hardware
---

# Hardware



## GPU support matrix

| Manufacturer | Driver                    | VR Support     | Reprojection Support           | Hybrid Graphics Support       | Notes                                                                                        |
|--------------|---------------------------|----------------|--------------------------------|-------------------------------|----------------------------------------------------------------------------------------------|
| Nvidia       | Nvidia (Closed Source)    | Good           | Functional                     | Supported                     | Requires an implicit [vulkan-layer](https://gitlab.freedesktop.org/monado/utilities/vulkan-layers) [*AUR package*](https://aur.archlinux.org/packages/monado-vulkan-layers-git) to run.           |
| Nvidia       | Nouveau (Open Source)     | Functional     | Poor                           | Supported                     | Lacks DisplayPort audio, suffers from stutter without reprojection queue. |
| Intel        | ANV (Open Source)         | Unknown        | N/A                            | Supported                     | Lacks direct mode implementation in driver, unable to drive wired HMDs.   |
| Intel        | Xe (Open Source)          | Useless        | N/A                            | N/A                           | Lacks direct mode implementation in driver, unable to drive wired HMDs.   |
| AMD          | RADV (Open Source)        | Excellent      | Robust (RDNA+)                 | Supported                     | RDNA generation and up supported with compute tunneling for reprojection. Lower than RDNA are not robust. |
| AMD          | AMDVLK (Open Source)      | Useless        | N/A                            | N/A                           | RADV preferred in all circumstances, unable to drive wired HMDs. Do not use. Do not seek support. |
| AMD          | AMDGPU PRO (Closed Source)| Useless        | N/A                            | N/A                           | RADV preferred in all circumstances, unable to drive wired HMDs. Do not use. Do not seek support. |

**Notes:**
- **VR Support**: Indicates how well supported the necessary Vulkan API components are.
- **Reprojection Support**: Describes the support and quality of reprojection features for VR. Poor support indicates that the driver is not able to properly handle Vulkan realtime shaders and it will present as visual stutter. Non-robust solutions will suffer stutter under very high GPU load.
- **PRIME/ Hybrid GPU Support**: Compatibility with systems using multiple GPUs for render offload. Monado and all clients must be run on a single select GPU due to memory tiling requirements.
- For Nvidia proprietary drivers, the [vulkan-layer](https://gitlab.freedesktop.org/monado/utilities/vulkan-layers) is **required** not to crash.
- AMD GPUs lower than RDNA generation have functional but less robust reprojection capabilities, expected to be similar to Intel.
- Audio over displayport is known to temporarily cut out when new audio sources spring up on pipewire [without a fix to add alsa headroom](https://wiki.archlinux.org/title/PipeWire#Audio_cutting_out_when_multiple_streams_start_playing)
- X11 configurations are discouraged but workable, please upgrade your system to Wayland if at all possible.



## XR Devices

A non-comprehensive table of various VR/XR devices and the drivers that support them.

| Device               | [SteamVR](/docs/steamvr/)             | [Monado](/docs/fossvr/monado/) | [WiVRn](/docs/fossvr/wivrn/) |
|----------------------|:-------------------------------------:|:------------------------------:|:----------------------------:|
| **PC VR**            |                                       |                                |                              |
| Valve Index          | ✅                                    | ✅                             | 🚧 (WiVRn PC-PC stream)      |
| HTC Vive             | ✅                                    | ✅                             | 🚧 (WiVRn PC-PC stream)      |
| HTC Vive Pro         | ✅                                    | ✅                             | 🚧 (WiVRn PC-PC stream)      |
| HTC Vive Pro Eye     | ✅ (No eyechip)                       | ✅ (No eyechip)                | 🚧 (WiVRn PC-PC stream)      |
| HTC Vive Pro 2       | ✅ (custom [driver and patches](https://github.com/CertainLach/VivePro2-Linux-Driver)) | ? (presumably with two kernel patches [1](https://github.com/CertainLach/VivePro2-Linux-Driver/blob/master/kernel-patches/0002-drm-edid-parse-DRM-VESA-dsc-bpp-target.patch) [2](https://github.com/CertainLach/VivePro2-Linux-Driver/blob/master/kernel-patches/0003-drm-amd-use-fixed-dsc-bits-per-pixel-from-edid.patch)) | --                           |
| Bigscreen Beyond     | ~ (Functional after monado has run)   | ✅ (with two kernel patches [1](https://github.com/CertainLach/VivePro2-Linux-Driver/blob/master/kernel-patches/0002-drm-edid-parse-DRM-VESA-dsc-bpp-target.patch) [2](https://github.com/CertainLach/VivePro2-Linux-Driver/blob/master/kernel-patches/0003-drm-amd-use-fixed-dsc-bits-per-pixel-from-edid.patch))                   | --                           |
| Somnium VR1          | ?                                     | ?                              | ?                            |
| VRgineers XTAL       | ?                                     | ?                              | ?                            |
| StarVR One           | ?                                     | ?                              | ?                            |
| Varjo VR-1           | ?                                     | ?                              | ?                            |
| Varjo VR-2           | ?                                     | ?                              | ?                            |
| Varjo VR-3           | ?                                     | ?                              | ?                            |
| Pimax 4K             | ❌ (Planned)                          | ❌ (Planned)                   | --                           |
| Pimax 5K Plus        | ❌ (Planned)                          | ❌ (Planned)                   | --                           |
| Pimax 8K             | ❌ (Planned)                          | ❌ (Planned)                   | --                           |
| Lenovo Explorer      | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| Acer AH101           | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| Dell Visor           | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| HP WMR headset       | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| Samsung Odyssey      | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| Asus HC102           | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| Samsung Odyssey+     | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| HP Reverb            | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| Acer OJO 500         | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| HP Reverb G2         | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| Oculus Rift CV1      | 🚧 (Monado SteamVR plugin)            | 🚧 (ancient openhmd based support) | 🚧 (WiVRn PC-PC stream)      |
| Oculus Rift S        | 🚧 (Monado SteamVR plugin)            | 🚧 (WIP)                       | 🚧 (WiVRn PC-PC stream)      |
| **Standalone**       |                                       |                                |                              |
| Quest                | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| Quest 2              | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| Quest Pro            | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| Quest 3              | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| Pico 4               | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| Pico Neo 3           | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| HTC Vive Focus 3     | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| HTC Vive XR Elite    | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| Lynx R1              | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| Apple Vision Pro     | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ❌                           |
| **Trackers**         |                                       |                                |                              |
| Vive/Tundra trackers | ✅                                    | ✅                             | ❌                           |
| SlimeVR trackers     | ✅                                    | ✅ (Supported by OSC)          | 🚧 (Proposed)                |
| Project Babble       | ✅ (oscavmgr)                         | ✅ (oscavmgr)                  | ✅ (oscavmgr)                |
| Eyetrack VR          | 🚧 (WIP)                              | 🚧 (WIP)                       | 🚧 (WIP)                     |
| Mercury Handtrack    | 🚧 (Monado SteamVR plugin, win only)  | ✅ (survive driver only)       | ❌                           |
| Lucid VR Gloves      | ?                                     | ✅ (survive driver only)       | ❌                           |
| Standable FBT        | ❌                                    | ❌                             | ❌                           |

## Hardware note

WiVRn PC XR to WiVRn PC client streaming remains mainly for debug use.

Vive Pro microphones should be set to use 44.1khz sample rates as feeding in 48khz raises the pitch of the audio.

Valve index audio output should be set to 48khz or no audio will output.

Vive Pro Eye HMD functional, eyechip WIP.

Pimax initialization code WIP.

Tundra trackers preferred for full body, linear and angular velocity stop upon occlusion.

Eyetrack VR and Project Babble will both be implemented through [oscavmgr](https://github.com/galister/oscavmgr) to emit proper unified flexes over OSC.