---
weight: 200
title: Quick Start
---

# Getting Started with SteamVR on Linux

## Check your Video Drivers

- {{< icon name="nvidia" >}} Nvidia users: use the version that's [officially listed as production](https://www.nvidia.com/en-us/drivers/unix/). new feature/beta versions tend to have issues. open driver recommended on 20 series cards or newer.
- {{< icon name="amd" >}} AMD users: use Mesa 24+ radeonsi, kernel 6.7.3+ if possible, stay away from amdpro.

## Dual-GPU Systems

- AMD+AMD laptop or desktop: Amazing choice, you have nothing to do.
- Desktop with iGPU and dGPU of different brands: Disable your iGPU in BIOS.
- Laptop with Nvidia dGPU: 
  - **Option 1 (Ubuntu-only)**: Try the official Ubuntu solution `prime-select nvidia`.
  - **Option 2**: Use `optimus-manager-qt` and switch to Nvidia.
  - **Option 3**: Launch steam, steamvr and all games with `prime-run` (Launch options: `prime-run %command%`)
  - **Extra tips**:
    - Use regular nvidia proprietary driver instead of nvidia-open.
    - Make ABSOLUTELY sure you don't have weird stuff like amdvlk, including lib32 installed when you have Nvidia (check `ls /usr/share/vulkan/icd.d`)
    - Stick to the PRODUCTION branch of Nvidia drivers (535 as of writing), newer ones may or may not work.

## Installing Steam

If the `steam` package is available on your distro, we recommend starting with that. For headsets using ALVR, the flatpak steam can work, but it's more of an advanced topic. ALVR docs can help you with that.

Launch Steam and install SteamVR. If it repeatedly nags you about sudo upon launch, you might want to setcap the vrcompositor:

```bash
sudo setcap CAP_SYS_NICE=eip ~/.local/share/Steam/steamapps/common/SteamVR/bin/linux64/vrcompositor-launcher
```

(Default install location on most distros)

## Setting up your HMD

**Valve Index, Vive, Vive Pro**: These will work just by plugging in.

**Vive Pro 2** will need the driver from here: <https://github.com/CertainLach/VivePro2-Linux-Driver>

If you use any of the above headsets with {{< icon name="nvidia" >}} Nvidia , you may get flickering or crashes. In this case, check your settings in `~/.steam/steam/config/steamvr.vrsettings`

Under the `"steamvr"` section:

```js
"allowSupersampleFiltering" : false,
"disableAsync" : true,
"enableLinuxVulkanAsync" : false,
```

For many games, you might want to turn on "Legacy Reprojection" in the per-app video settings.

**Quest 1/2/3/Pro, Pico 3/4, Vive Focus 3/XR Elite**:

Your best bet is [the latest ALVR release](https://github.com/alvr-org/ALVR/releases)

Also check the [ALVR wiki](https://github.com/alvr-org/ALVR/wiki)

## Auto-Restart Script

The `startvr` script [here](https://gist.github.com/galister/a85135f4a3aca5208ba4091069ab2222) is meant to make launching SteamVR less tedious.

It can be used with both ALVR and wired headsets.

What it does:
- Whitelist drivers for SpaceCal and ALVR so these will never get blocked
- Apply bindings spam patch by Plyshka
- Setcap the compositor after a SteamVR update
- Start SteamVR and restart it for you in case of a crash

Extra steps when using with ALVR:
- Under `Installation` tab, register the ALVR driver
- Under `Settings`, `Steamvr Launcher` section, set `Driver launch action` to `No action`.

None: Cloud sync can get stuck and prevent SteamVR from launching. I recommend disabling cloud sync.

If you've installed Steam in a non-standard location, change the variable on the top of the script.

To use the script, simply launch it from a terminal. It will ask for sudo password in case your SteamVR has been freshly installed or updated and it needs to `setcap` the compositor. ALVR-only: If you get a SteamVR error after this, open ALVR dashboard and launch SteamVR from there once.

If you need to restart SteamVR, simply exit it using your overlay and it'll be restarted for you. To really exit SteamVR, first Ctrl-C the script.
