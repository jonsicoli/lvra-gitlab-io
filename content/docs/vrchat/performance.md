---
weight: 60
title: Performance
---

# Performance

VRChat is notorius for not looking super great compared to modern 2D titles, while also being laggy. Here are some tips for getting a smoother experience.

# Recommended Settings

This gives a good baseline for anyone to start with. Feel free to tweak and experiment further.

- Anti-Aliasing: Off or 2x
  - Greatly increases GPU load (even more so on NVidia)
  - Try with off first, if the jagged edges bother you too much, try 2x.
- Pixel light count: Low
  - Each pixel light adds significant CPU load.
  - It's extemely common for new world creators to forget to bake lighting and upload a world with too many realtime lights, killing everyone's performance.
  - Turning these completely off will make some worlds look incorrect (too dark).
- Shadow Quality: Low
  - Peformance hit can be disproportionately high for visual benefit
- LOD Quality: Low-Medium
  - Only makes a real difference in complex scenes, such as forests, cityscapes, etc.
  - Adds GPU load based on how complex the scene is
- Particle Limiter: On

# CPU Bottlenecks

In VRC, it's very easy to hit a CPU bottleneck. Avatar material count, avatar mesh count, avatar animators, pixel lights, phys bones, particles all contribute to CPU load.

By far the biggest contributors are unoptimized avatars and unoptimized worlds.

## How to check if you're bottlenecked by CPU or GPU:

1. Check GPU usage in `nvtop` (any GPU) or `nvidia-smi` (NVidia-only).
  - If it's not at 100%, you're likely CPU bottlenecked.
2. SteamVR: Lower the SteamVR render resolution.
  - If your FPS doesn't increase, you're likely CPU bottlenecked.

## What to do when CPU bottlenecked

Here are some tips to ease VRChat's CPU hit on your system.

### Reduce Maximum Shown Avatars
I recommend 10-15 as a base setting, likely won't be interacting with more people than that at the same time.

### Block by default, utilize Show Avatar

1. Set up a custom Safety profile where you block Animators and Shaders for everyone (possibly even for friends).
2. Block Poorly Optimized Avatars: Very Poor
3. Manually `Show Avatar` people who you are actively interacting with.

This gives you full control on what you're seeing, and will be able to quickly identify that one person whose avatar is wrecking your FPS.

# Avatar Optimization

Are you running around in a Very Poor avatar? Consider this a read. (And also poke your friends to do the same!)

There are tools that let you do this in a few clicks, even if you are completely clueless about what to do.

## d4rkAvatarOptimizer
Install: `vrc-get repo add https://d4rkc0d3r.github.io/vpm-repos/main.json`

[Guide](https://github.com/d4rkc0d3r/d4rkAvatarOptimizer)


## Avatar Optimizer by Anatawa12
Install: `vrc-get repo add https://vpm.anatawa12.com/vpm.json`

[Guide](https://vpm.anatawa12.com/avatar-optimizer/en/docs/tutorial/basic-usage/)
